package com.danisproductive.nofi.manager.process;

import com.danisproductive.nofi.core.process.JavaNoFiProcess;
import com.danisproductive.nofi.core.process.NoFiProcess;
import com.danisproductive.nofi.core.process.NoFiProcessDictionary;
import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.Map;

public class NoFiProcessFactory {
    private static final String DEFAULT_PROCESS_TYPE_CLASSPATH = JavaNoFiProcess.class.getCanonicalName();
    private static final String CONFIG_TYPE = "type";
    private static final Map<String, NoFIProcessCreator<? extends NoFiProcess>> processFactoryMap = new HashMap<>();

    static {
        processFactoryMap.put(JavaNoFiProcess.class.getCanonicalName(), JavaNoFiProcess::new);
    }

    public static NoFiProcess create(JsonObject config) {
        String processType = null;
        if (config.has(CONFIG_TYPE)) {
            processType = config.get(CONFIG_TYPE).getAsString();
        }

        NoFiProcess process = create(processType);
        if (process != null) {
            NoFiProcessUpdater.updateProcessFields(process, config);
        }

        return process;
    }

    public static NoFiProcess create(String processClassPath) {
        if (processClassPath == null) {
            processClassPath = DEFAULT_PROCESS_TYPE_CLASSPATH;
        }

        if (!processClassPath.contains(".")) {
            processClassPath = NoFiProcessDictionary.translate(processClassPath);
        }

        if (processFactoryMap.containsKey(processClassPath)) {
            return processFactoryMap.get(processClassPath).create();
        } else {
            return null;
        }

    }

    public static NoFiProcess create() {
        return create((String) null);
    }

    @FunctionalInterface
    private interface NoFIProcessCreator <T extends NoFiProcess> {
        T create();
    }
}
