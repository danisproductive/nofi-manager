package com.danisproductive.nofi.manager.process;

import com.danisproductive.nofi.core.input.NoFiInput;
import com.danisproductive.nofi.core.output.NoFiOutput;
import com.danisproductive.nofi.core.process.NoFiProcess;
import com.danisproductive.nofi.core.queue.NoFiQueue;
import com.danisproductive.nofi.core.queue.NoFiQueueFactory;
import com.danisproductive.nofi.manager.queue.kafka.NoFiQueueServerConfig;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.Map;

public class NoFiProcessUpdater {
    private static final String CONFIG_NAME_PROPERTY = "name";
    private static final String CONFIG_SCHEDULER_PROPERTY = "scheduler";
    private static final String CONFIG_INPUTS_PROPERTY = "inputs";
    private static final String CONFIG_OUTPUTS_PROPERTY = "outputs";
    private static final String CONFIG_OUTPUTS_NAME_PROPERTY = "name";
    private static final String CONFIG_IO_RENAME_NEW = "newName";
    private static final String CONFIG_IO_RENAME_ORIGINAL = "originalName";
    private static final String CONFIG_TAGS_PROPERTY = "tags";


    private static final NoFiQueueServerConfig queueServerConfig =
            new NoFiQueueServerConfig("localhost:9092", "my-group-id");
    public static final Map<String, NoFiProcessFieldUpdater> processFiledUpdaters = new HashMap<>();

    static {
        processFiledUpdaters.put(CONFIG_NAME_PROPERTY, NoFiProcessUpdater::name);
        processFiledUpdaters.put(CONFIG_SCHEDULER_PROPERTY, NoFiProcessUpdater::scheduler);
        processFiledUpdaters.put(CONFIG_INPUTS_PROPERTY, NoFiProcessUpdater::inputs);
        processFiledUpdaters.put(CONFIG_OUTPUTS_PROPERTY, NoFiProcessUpdater::outputs);
        processFiledUpdaters.put(CONFIG_TAGS_PROPERTY, NoFiProcessUpdater::tags);
    }

    public static void updateProcessFields(NoFiProcess process, JsonObject config) {
        NoFiProcessUpdater.processFiledUpdaters.forEach((property, updater) -> {
            if (config.has(property)) {
                updater.update(process, config);
            }
        });
    }

    private static void name(NoFiProcess process, JsonObject config) {
        process.setName(config.get(CONFIG_NAME_PROPERTY).getAsString());
    }

    private static void scheduler(NoFiProcess process, JsonObject config) {
        JsonObject schedulerConfig = config.get(CONFIG_SCHEDULER_PROPERTY).getAsJsonObject();
        process.setScheduler(schedulerConfig);
    }

    private static void inputs(NoFiProcess process, JsonObject config) {
        JsonArray inputs = config.get(CONFIG_INPUTS_PROPERTY).getAsJsonArray();
        inputs.forEach(input -> {
            // If the input config is an object then this is a request to update an existing input (rename)
            if (input.isJsonObject()) {
                updateInputName(process, input.getAsJsonObject());
            } else {
                process.addInput(input.getAsString());
            }
        });
    }

    private static void updateInputName(NoFiProcess process, JsonObject inputConfig) {
        if (inputConfig.has(CONFIG_IO_RENAME_NEW)) {
            String newName = inputConfig.get(CONFIG_IO_RENAME_NEW).getAsString();
            String originalName = inputConfig.get(CONFIG_IO_RENAME_ORIGINAL).getAsString();
            NoFiInput processInput = process.getInputByName(inputConfig.get(originalName).getAsString());
            processInput.setName(newName);
        } else {
            process.addInput(inputConfig.get(CONFIG_NAME_PROPERTY).getAsString());
        }
    }

    private static void outputs(NoFiProcess process, JsonObject config) {
        JsonArray outputs = config.get(CONFIG_OUTPUTS_PROPERTY).getAsJsonArray();
        outputs.forEach(outputConfigElement -> {
            JsonObject outputConfig = outputConfigElement.getAsJsonObject();
            //todo update to consts
            if (outputConfig.has("updateType")) {
                String updateType = outputConfig.get("updateType").getAsString();

                if (updateType.equals("rename")) {
                    updateOutputName(process, outputConfig);

                    return;
                }
            }

            //todo use result from addNewOutput
            addNewOutput(process, outputConfig);
        });
    }

    private static void updateOutputName(NoFiProcess process, JsonObject outputConfig) {
        String newName = outputConfig.get(CONFIG_IO_RENAME_NEW).getAsString();
        String originalName = outputConfig.get(CONFIG_IO_RENAME_ORIGINAL).getAsString();
        NoFiOutput output = process.getOutputByName(originalName);
        output.setName(newName);
    }

    private static boolean addNewOutput(NoFiProcess process, JsonObject outputConfig) {
        NoFiOutput output = process.addOutput(outputConfig.get(CONFIG_OUTPUTS_NAME_PROPERTY).getAsString());
        //todo implement this
        NoFiQueue queue = NoFiQueueFactory.getInstance().create(outputConfig, queueServerConfig.getKafkaConfig());

        if (queue != null) {
            output.setQueue(queue);

            return true;
        }

        return false;
    }

    private static void tags(NoFiProcess process, JsonObject config) {
        JsonArray tags = config.get(CONFIG_TAGS_PROPERTY).getAsJsonArray();
        tags.forEach(tag -> process.tag(tag.getAsString()));
    }

    @FunctionalInterface
    private interface NoFiProcessFieldUpdater {
        void update(NoFiProcess process, JsonObject config);
    }
}

