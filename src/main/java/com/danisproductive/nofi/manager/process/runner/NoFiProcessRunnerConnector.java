package com.danisproductive.nofi.manager.process.runner;

import com.danisproductive.nofi.core.process.NoFiProcess;

public class NoFiProcessRunnerConnector {
    private String baseUrl;

    public NoFiProcessRunnerConnector(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void startProcessOnRunner(NoFiProcess process) {
        //TODO
    }

    public void stopProcessOnRunner(NoFiProcess process) {
        //TODO
    }
}
