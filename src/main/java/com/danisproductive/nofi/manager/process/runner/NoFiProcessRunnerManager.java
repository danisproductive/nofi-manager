package com.danisproductive.nofi.manager.process.runner;

import com.danisproductive.nofi.core.process.NoFiProcess;

import java.util.List;

public class NoFiProcessRunnerManager {
    private final List<NoFiProcessRunnerConnector> processRunnerConnectors;
    private int nextProcessRunnerIndex;

    public NoFiProcessRunnerManager(List<NoFiProcessRunnerConnector> processRunnerConnectors) {
        this.processRunnerConnectors = processRunnerConnectors;
        nextProcessRunnerIndex = 0;
    }

    private void advanceProcessRunnerIndex() {
        nextProcessRunnerIndex = (nextProcessRunnerIndex + 1) % processRunnerConnectors.size();
    }

    public void startProcessOnRunner(NoFiProcess process) {
        processRunnerConnectors.get(nextProcessRunnerIndex).startProcessOnRunner(process);
        advanceProcessRunnerIndex();
    }

    public void stopProcessOnRunner(NoFiProcess process) {
        processRunnerConnectors.get(nextProcessRunnerIndex).startProcessOnRunner(process);
        advanceProcessRunnerIndex();
    }
}
