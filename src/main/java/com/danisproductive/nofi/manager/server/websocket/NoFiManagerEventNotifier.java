package com.danisproductive.nofi.manager.server.websocket;

import com.danisproductive.nofi.core.output.NoFiOutput;
import com.danisproductive.nofi.core.process.NoFiProcess;
import com.danisproductive.nofi.core.server.websocket.NoFiWebSocketServer;
import com.danisproductive.nofi.manager.json.ManagerJsonConverter;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.List;

import static com.danisproductive.nofi.core.server.websocket.NoFiWebSocketMessageProperty.*;
import static com.danisproductive.nofi.core.server.websocket.NoFiWebSocketMessageType.*;

public class NoFiManagerEventNotifier {
    private static final NoFiWebSocketServer webSocketServer = new NoFiWebSocketServer(3001);

    public static void startWebSocket() {
        webSocketServer.run();
    }

    public static void sendProcessAdded(NoFiProcess process) {
        JsonObject requestBody = new JsonObject();
        requestBody.add(PROCESS_CONFIG.getValue(), ManagerJsonConverter.gson.toJsonTree(process));
        webSocketServer.broadcast(PROCESS_ADDED, requestBody);
    }

    public static void sendProcessUpdated(NoFiProcess process) {
        JsonObject requestBody = new JsonObject();
        requestBody.add(PROCESS_CONFIG.getValue(), ManagerJsonConverter.gson.toJsonTree(process));
        webSocketServer.broadcast(PROCESS_UPDATED, requestBody);
    }

    public static void sendDisconnectProcess(String targetProcessId, String inputName) {
        JsonObject requestBody = new JsonObject();
        requestBody.addProperty(TARGET_PROCESS_ID.getValue(), targetProcessId);
        requestBody.addProperty(INPUT_NAME.getValue(), inputName);
        webSocketServer.broadcast(PROCESS_DISCONNECTED, requestBody);
    }

    public static void sendConnectProcess(
            String sourceProcessId,
            String sourceOutputName,
            String targetProcessId,
            String targetInputName) {
        JsonObject requestBody = new JsonObject();
        requestBody.addProperty(SOURCE_PROCESS_ID.getValue(), sourceProcessId);
        requestBody.addProperty(SOURCE_OUTPUT_NAME.getValue(), sourceOutputName);
        requestBody.addProperty(TARGET_PROCESS_ID.getValue(), targetProcessId);
        requestBody.addProperty(TARGET_INPUT_NAME.getValue(), targetInputName);
        webSocketServer.broadcast(PROCESS_CONNECTED, requestBody);
    }

    public static void sendStartProcess(String processId) {
        JsonObject requestBody = new JsonObject();
        requestBody.addProperty(TARGET_PROCESS_ID.getValue(), processId);
        webSocketServer.broadcast(PROCESS_STARTED, requestBody);
    }

    public static void sendStopProcess(String processId) {
        JsonObject requestBody = new JsonObject();
        requestBody.addProperty(TARGET_PROCESS_ID.getValue(), processId);
        webSocketServer.broadcast(PROCESS_STOPPED, requestBody);
    }

    public static void sendInputDeleted(String processId, String inputName) {
        JsonObject requestBody = new JsonObject();
        requestBody.addProperty(PROCESS_ID.getValue(), processId);
        requestBody.addProperty(INPUT_NAME.getValue(), inputName);
        webSocketServer.broadcast(INPUT_ADDED, requestBody);
    }

    public static void sendOutputsDeleted(String processId, List<NoFiOutput> outputs) {
        JsonObject requestBody = new JsonObject();
        JsonArray deletedOutputs = new JsonArray();

        outputs.forEach(output -> {
            JsonObject deletedOutput = new JsonObject();
            deletedOutput.addProperty(PROCESS_ID.getValue(), processId);
            deletedOutput.addProperty(OUTPUT_NAME.getValue(), output.getName());
            deletedOutputs.add(deletedOutput);
        });

        requestBody.addProperty(OUTPUTS_DELETED.getValue(), OUTPUTS_DELETED.getValue());
        webSocketServer.broadcast(OUTPUTS_DELETED, requestBody);
    }

    public static void sendProcessDelete(NoFiProcess process) {
        JsonObject requestBody = new JsonObject();
        requestBody.addProperty(PROCESS_ID.getValue(), process.getId());
        sendOutputsDeleted(process.getId(), process.getOutputs());
        webSocketServer.broadcast(PROCESS_DELETED, requestBody);
    }
}
