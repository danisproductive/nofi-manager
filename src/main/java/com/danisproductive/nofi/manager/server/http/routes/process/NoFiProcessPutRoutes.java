package com.danisproductive.nofi.manager.server.http.routes.process;

import com.danisproductive.nofi.core.process.NoFiProcess;
import com.danisproductive.nofi.core.process.NoFiProcessValidator;
import com.danisproductive.nofi.core.utils.ValidationResult;
import com.danisproductive.nofi.manager.NoFiManager;
import com.danisproductive.nofi.manager.json.ManagerJsonConverter;
import com.danisproductive.nofi.manager.process.NoFiProcessUpdater;
import com.danisproductive.nofi.manager.server.websocket.NoFiManagerEventNotifier;
import com.google.gson.JsonObject;
import io.javalin.Javalin;

import static com.danisproductive.nofi.manager.server.http.routes.process.NoFiProcessServerRoutes.*;

public class NoFiProcessPutRoutes {
    private static final String PARAM_SOURCE_PROCESS_ID = "sourceProcessId";
    private static final String PARAM_SOURCE_OUTPUT_NAME = "outputName";
    private static final String PARAM_TARGET_PROCESS_ID = "targetProcessId";

    public void setPutRoutes(Javalin server, NoFiManager manager) {
        setUpdateProcessRoute(server, manager);
        setStartProcessRoute(server, manager);
        setStopProcessRoute(server, manager);
        setConnectProcessRoute(server, manager);
        setDisconnectProcessRoute(server, manager);
    }

    private void setDisconnectProcessRoute(Javalin server, NoFiManager manager) {
        server.put(PROCESS_ROUTE + "/disconnect", (context) -> {
            String targetProcessID = context.queryParam(PARAM_TARGET_PROCESS_ID);
            String inputName = context.queryParam(PARAM_INPUT_NAME);
            NoFiProcess targetProcess = manager.getProcess(targetProcessID);
            targetProcess.getInputByName(inputName).disconnect();
            NoFiManagerEventNotifier.sendDisconnectProcess(targetProcessID, inputName);
        });
    }

    private void setConnectProcessRoute(Javalin server, NoFiManager manager) {
        server.put(PROCESS_ROUTE + "connect", (context) -> {
            String sourceProcessID = context.queryParam(PARAM_SOURCE_PROCESS_ID);
            String outputName = context.queryParam(PARAM_SOURCE_OUTPUT_NAME);
            String targetProcessID = context.queryParam(PARAM_TARGET_PROCESS_ID);
            String inputName = context.queryParam(PARAM_INPUT_NAME);
            NoFiProcess targetProcess = manager.getProcess(targetProcessID);
            targetProcess.getInputByName(inputName).connect(sourceProcessID, outputName);
            NoFiManagerEventNotifier.sendConnectProcess(
                    targetProcessID,
                    inputName,
                    sourceProcessID,
                    outputName);
        });
    }

    private void setStartProcessRoute(Javalin server, NoFiManager manager) {
        server.put(PROCESS_ROUTE + "/start", context -> {
            String processId = context.queryParam(PARAM_PROCESS_ID);
            NoFiProcess process = manager.getProcess(processId);

            if (!process.isRunning()) {
                process.setRunning(true);
                NoFiManagerEventNotifier.sendStartProcess(processId);
            }
        });
    }

    private void setStopProcessRoute(Javalin server, NoFiManager manager) {
        server.put(PROCESS_ROUTE + "/stop", context -> {
            String processId = context.queryParam(PARAM_PROCESS_ID);
            NoFiProcess process = manager.getProcess(processId);

            if (process.isRunning()) {
                process.setRunning(false);
                NoFiManagerEventNotifier.sendStopProcess(processId);
            }
        });
    }

    private void setUpdateProcessRoute(Javalin server, NoFiManager manager) {
        server.put(PROCESS_ROUTE, context -> {
            JsonObject config = ManagerJsonConverter.gson.fromJson(
                    context.queryParam(PARAM_PROCESS_CONFIG),
                    JsonObject.class
            );

            ValidationResult validation = NoFiProcessValidator.validate(config);

            if (validation.isValid()) {
                NoFiProcess process = manager.getProcess(config.get(PROCESS_ID).getAsString());

                if (process != null) {
                    NoFiProcessUpdater.updateProcessFields(process, config);
                    NoFiManagerEventNotifier.sendProcessUpdated(process);
                } else {
                    context.result("process " + config.get(PROCESS_ID).getAsString() + "not found");
                    context.status(422);
                }
            } else {
                context.result(validation.getMessage());
                context.status(422);
            }
        });
    }
}
