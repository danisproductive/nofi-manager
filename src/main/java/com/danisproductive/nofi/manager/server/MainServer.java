package com.danisproductive.nofi.manager.server;

import com.danisproductive.nofi.manager.server.http.NoFiManagerServer;
import com.danisproductive.nofi.manager.server.websocket.NoFiManagerEventNotifier;

public class MainServer {
    public static void main(String[] args) {
        new NoFiManagerServer().start();
        NoFiManagerEventNotifier.startWebSocket();
    }
}
