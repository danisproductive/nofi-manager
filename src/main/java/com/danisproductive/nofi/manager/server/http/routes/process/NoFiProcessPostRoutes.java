package com.danisproductive.nofi.manager.server.http.routes.process;

import com.danisproductive.nofi.core.process.NoFiProcess;
import com.danisproductive.nofi.core.process.NoFiProcessValidator;
import com.danisproductive.nofi.core.utils.ValidationResult;
import com.danisproductive.nofi.manager.NoFiManager;
import com.danisproductive.nofi.manager.json.ManagerJsonConverter;
import com.danisproductive.nofi.manager.process.NoFiProcessFactory;
import com.danisproductive.nofi.manager.server.websocket.NoFiManagerEventNotifier;
import com.google.gson.JsonObject;
import io.javalin.Javalin;

import static com.danisproductive.nofi.manager.server.http.routes.process.NoFiProcessServerRoutes.PARAM_PROCESS_CONFIG;
import static com.danisproductive.nofi.manager.server.http.routes.process.NoFiProcessServerRoutes.PROCESS_ROUTE;

public class NoFiProcessPostRoutes {
    public void setPostRoutes(Javalin server, NoFiManager manager) {
        setPostProcessRoute(server, manager);
    }

    private void setPostProcessRoute(Javalin server, NoFiManager manager) {
        server.post(PROCESS_ROUTE, context -> {
            JsonObject config = ManagerJsonConverter.gson.fromJson(
                    context.queryParam(PARAM_PROCESS_CONFIG),
                    JsonObject.class
            );

            ValidationResult validation = NoFiProcessValidator.validate(config);

            if (validation.isValid()) {
                NoFiProcess process = NoFiProcessFactory.create(config);
                if (process != null) {
                    manager.addProcess(process);
                    NoFiManagerEventNotifier.sendProcessAdded(process);

                    return;
                } else {
                    validation.addMessage("Process type does not exist");
                }
            }

            context.result(validation.getMessage());
            context.status(422);
        });
    }
}
