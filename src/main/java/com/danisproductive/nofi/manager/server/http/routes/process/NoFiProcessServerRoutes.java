package com.danisproductive.nofi.manager.server.http.routes.process;

import com.danisproductive.nofi.manager.NoFiManager;
import io.javalin.Javalin;

public class NoFiProcessServerRoutes {
    public static final String PROCESS_ROUTE = "process";
    public static final String PARAM_PROCESS_CONFIG = "config";
    public static final String PARAM_PROCESS_ID = "processId";
    public static final String PARAM_TAG_NAME = "tagName";
    public static final String PARAM_INPUT_NAME = "inputName";
    public static final String PARAM_OUTPUT_NAME = "outputName";
    public static final String PROCESS_ID = "id";

    private final NoFiProcessGetRoutes processGetRoutes;
    private final NoFiProcessPostRoutes processPostRoutes;
    private final NoFiProcessPutRoutes processPutRoutes;
    private final NoFiProcessDeleteRoutes processDeleteRoutes;

    public NoFiProcessServerRoutes() {
        processGetRoutes = new NoFiProcessGetRoutes();
        processPostRoutes = new NoFiProcessPostRoutes();
        processPutRoutes = new NoFiProcessPutRoutes();
        processDeleteRoutes = new NoFiProcessDeleteRoutes();
    }

    public void setRoutes(Javalin server, NoFiManager manager) {
        processGetRoutes.setGetRoutes(server, manager);
        processPostRoutes.setPostRoutes(server, manager);
        processPutRoutes.setPutRoutes(server, manager);
        processDeleteRoutes.setDeleteRoutes(server, manager);
        server.after(PROCESS_ROUTE, context -> manager.save());
    }
}
