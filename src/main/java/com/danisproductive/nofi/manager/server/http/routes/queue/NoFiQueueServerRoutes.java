package com.danisproductive.nofi.manager.server.http.routes.queue;

import com.danisproductive.nofi.manager.queue.QueueManager;
import io.javalin.Javalin;

public class NoFiQueueServerRoutes {
    private static final String PARAM_QUEUE_NAME = "queueName";
    private static final String PARAM_QUEUE_CONTENT = "content";

    public void setRoutes(Javalin server, QueueManager queueManager) {
        setPushRoute(server, queueManager);
        setPullRoute(server, queueManager);
        setPeekRoute(server, queueManager);
    }

    private void setPushRoute(Javalin server, QueueManager queueManager) {
        server.put("/queue/push", (context) -> {
            String queueName = context.queryParam(PARAM_QUEUE_NAME);
            String dataToPush = context.queryParam(PARAM_QUEUE_CONTENT);
            queueManager.getQueue(queueName).push(dataToPush);
        });
    }

    private void setPullRoute(Javalin server , QueueManager queueManager) {
        server.get("/queue/pull", (context) -> {
            String queueName = context.queryParam(PARAM_QUEUE_NAME);
            context.result(queueManager.getQueue(queueName).pull().toString());
        });
    }

    private void setPeekRoute(Javalin server, QueueManager queueManager) {
        server.get("/queue/peek", (context) -> {
            String queueName = context.queryParam(PARAM_QUEUE_NAME);
            context.result(queueManager.getQueue(queueName).peek().toString());
        });
    }
}
