package com.danisproductive.nofi.manager.server.http.routes.config;

import com.danisproductive.nofi.manager.json.ManagerJsonConverter;
import com.danisproductive.nofi.manager.process.runner.NoFiProcessRunnerConnector;
import com.danisproductive.nofi.manager.server.http.NoFiManagerServer;
import io.javalin.Javalin;
import io.javalin.http.Context;


public class ConfigRoutes {
    private static final String ROOT_ROUTE = "/config";
    private static final String PROCESS_RUNNERS_ROUTE = ROOT_ROUTE + "/process-runner";
    private static final String PARAM_RUNNER_CONFIG = "processRunner";


    public void setRoutes(Javalin server, NoFiManagerServer nofiServer) {
        setPostRoutes(server, nofiServer);
        setDeleteRoutes(server, nofiServer);
        server.after(PROCESS_RUNNERS_ROUTE, context -> nofiServer.saveConfig());

    }

    private void setPostRoutes(Javalin server, NoFiManagerServer nofiServer) {
        server.post(PROCESS_RUNNERS_ROUTE, context -> {
            NoFiProcessRunnerConnector noFiProcessRunnerConnector = getProcessRunnerConnectorFromRequest(context);
            nofiServer.addProcessRunner(noFiProcessRunnerConnector);
        });
    }

    private void setDeleteRoutes(Javalin server, NoFiManagerServer nofiServer) {
        server.delete(PROCESS_RUNNERS_ROUTE, context -> {
            NoFiProcessRunnerConnector noFiProcessRunnerConnector = getProcessRunnerConnectorFromRequest(context);
            nofiServer.removeProcessRunner(noFiProcessRunnerConnector);
        });
    }

    private NoFiProcessRunnerConnector getProcessRunnerConnectorFromRequest(Context context) {
        return ManagerJsonConverter.gson
                .fromJson(context.queryParam(PARAM_RUNNER_CONFIG), NoFiProcessRunnerConnector.class);
    }
}
