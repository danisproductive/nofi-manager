package com.danisproductive.nofi.manager.server.http;

import com.danisproductive.nofi.core.utils.config.FileConfigReader;
import com.danisproductive.nofi.manager.NoFiManager;
import com.danisproductive.nofi.manager.json.ManagerJsonConverter;
import com.danisproductive.nofi.manager.process.runner.NoFiProcessRunnerConnector;
import com.danisproductive.nofi.manager.process.runner.NoFiProcessRunnerManager;
import com.danisproductive.nofi.manager.queue.QueueManager;
import com.danisproductive.nofi.manager.server.http.routes.config.ConfigRoutes;
import com.danisproductive.nofi.manager.server.http.routes.process.NoFiProcessServerRoutes;
import com.danisproductive.nofi.manager.server.http.routes.queue.NoFiQueueServerRoutes;
import com.danisproductive.nofi.manager.server.http.routes.tag.NoFiTagServerRoutes;
import com.danisproductive.nofi.core.server.http.NoFiServer;
import io.javalin.Javalin;

import java.io.FileNotFoundException;
import java.util.List;

public class NoFiManagerServer extends NoFiServer<NoFiManagerServerConfig> {
    private NoFiManager noFiManager;
    private final QueueManager queueManager;
    private final NoFiProcessRunnerManager processRunnerManager;
    private final ConfigRoutes configRoutes;
    private final NoFiProcessServerRoutes processRoutes;
    private final NoFiTagServerRoutes tagRoutes;
    private final NoFiQueueServerRoutes queueRoutes;


    public NoFiManagerServer() {
        super("manager-server.json", NoFiManagerServerConfig.class, ManagerJsonConverter.gson);
        try {
            noFiManager = new FileConfigReader<NoFiManager>(ManagerJsonConverter.gson)
                    .read("nofi-manager-data.json", NoFiManager.class);
        } catch (FileNotFoundException e) {
            noFiManager = new NoFiManager();
        }

        processRunnerManager = new NoFiProcessRunnerManager(super.config.getProcessRunners());
        queueManager = new QueueManager();
        initializeQueues();

        configRoutes = new ConfigRoutes();
        processRoutes = new NoFiProcessServerRoutes();
        tagRoutes = new NoFiTagServerRoutes();
        queueRoutes = new NoFiQueueServerRoutes();
    }

    private void initializeQueues() {
        noFiManager.getProcesses().values().forEach(process ->
                process.getOutputs().forEach(output -> queueManager.addQueue(output.getQueue())));
    }

    @Override
    protected void setRoutes(Javalin server) {
        configRoutes.setRoutes(server, this);
        processRoutes.setRoutes(server, noFiManager);
        tagRoutes.setRoutes(server, noFiManager);
        queueRoutes.setRoutes(server, queueManager);
    }

    public void addProcessRunner(NoFiProcessRunnerConnector processRunnerConnectors) {
        if (processRunnerConnectors != null) {
            List<NoFiProcessRunnerConnector> processRunners = super.config.getProcessRunners();

            if (processRunnerExists(processRunnerConnectors, processRunners)) {
                processRunners.add(processRunnerConnectors);
            }
        }
    }

    private boolean processRunnerExists(NoFiProcessRunnerConnector processRunnerConfig,
                                        List<NoFiProcessRunnerConnector> processRunners) {
        return processRunners.stream()
                .noneMatch(runner -> runner.getBaseUrl().equals(processRunnerConfig.getBaseUrl()));
    }

    public void removeProcessRunner(NoFiProcessRunnerConnector noFiProcessRunnerConnector) {
        super.config.getProcessRunners().remove(noFiProcessRunnerConnector);
    }
}
