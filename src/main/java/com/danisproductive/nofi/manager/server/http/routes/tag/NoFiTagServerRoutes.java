package com.danisproductive.nofi.manager.server.http.routes.tag;

import com.danisproductive.nofi.manager.NoFiManager;
import com.google.gson.Gson;
import io.javalin.Javalin;

public class NoFiTagServerRoutes {
    private static final String CONF_TAG_NAME = "name";

    private Gson gson;

    public NoFiTagServerRoutes() {
        gson = new Gson();
    }

    public void setRoutes(Javalin server, NoFiManager manager) {
        setGetRoutes(server, manager);
        setPostRoutes(server, manager);
        setDeleteRoutes(server, manager);
    }

    private void setDeleteRoutes(Javalin server, NoFiManager manager) {
        setDeleteTagRoute(server, manager);
    }

    private void setDeleteTagRoute(Javalin server, NoFiManager manager) {
        server.delete("/tag", context -> {
            String tagName = context.queryParam(CONF_TAG_NAME);
            manager.removeTag(tagName);
        });
    }

    private void setPostRoutes(Javalin server, NoFiManager manager) {
        setPostTagRoute(server, manager);
    }

    private void setPostTagRoute(Javalin server, NoFiManager manager) {
        server.post("/tag", context -> {
            String tagName = context.queryParam(CONF_TAG_NAME);
            manager.createTag(tagName);
        });
    }

    private void setGetRoutes(Javalin server, NoFiManager manager) {
        setGetTagsRoute(server, manager);
    }

    private void setGetTagsRoute(Javalin server, NoFiManager manager) {
        server.get("/tags", context -> {
            context.result(gson.toJson(manager.getTags()));
        });
    }
}
