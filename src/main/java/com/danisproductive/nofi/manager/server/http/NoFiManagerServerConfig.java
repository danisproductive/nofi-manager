package com.danisproductive.nofi.manager.server.http;

import com.danisproductive.nofi.core.server.http.NoFiServerConfig;
import com.danisproductive.nofi.manager.process.runner.NoFiProcessRunnerConnector;

import java.util.List;

public class NoFiManagerServerConfig extends NoFiServerConfig {
    private String nofiQueueURL;
    private List<NoFiProcessRunnerConnector> processRunners;

    public String getNofiQueueURL() {
        return nofiQueueURL;
    }

    public void setNofiQueueURL(String nofiQueueURL) {
        this.nofiQueueURL = nofiQueueURL;
    }

    public List<NoFiProcessRunnerConnector> getProcessRunners() {
        return processRunners;
    }

    public void setProcessRunners(List<NoFiProcessRunnerConnector> processRunners) {
        this.processRunners = processRunners;
    }
}
