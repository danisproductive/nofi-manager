package com.danisproductive.nofi.manager.server.http.routes.process;

import com.danisproductive.nofi.core.process.NoFiProcess;
import com.danisproductive.nofi.manager.NoFiManager;
import com.danisproductive.nofi.manager.json.ManagerJsonConverter;
import io.javalin.Javalin;

import java.util.Map;

import static com.danisproductive.nofi.manager.server.http.routes.process.NoFiProcessServerRoutes.PROCESS_ROUTE;

public class NoFiProcessGetRoutes {
    public void setGetRoutes(Javalin server, NoFiManager manager) {
        setGetProcessRoute(server, manager);
        setGetProcessesRoute(server, manager);
    }

    private void setGetProcessRoute(Javalin server, NoFiManager manager) {
        server.get(PROCESS_ROUTE, context -> {
            String processId = context.queryParam(NoFiProcessServerRoutes.PARAM_PROCESS_ID);
            NoFiProcess process = manager.getProcess(processId);
            context.result(ManagerJsonConverter.gson.toJson(process));
        });
    }

    private void setGetProcessesRoute(Javalin server, NoFiManager manager) {
        server.get("/processes", context -> {
            Map<String, NoFiProcess> processes = manager.getProcesses();
            context.result(ManagerJsonConverter.gson.toJson(processes));
        });
    }
}
