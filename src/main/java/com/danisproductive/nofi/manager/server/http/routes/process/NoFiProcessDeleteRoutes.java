package com.danisproductive.nofi.manager.server.http.routes.process;

import com.danisproductive.nofi.core.process.NoFiProcess;
import com.danisproductive.nofi.manager.NoFiManager;
import com.danisproductive.nofi.manager.server.websocket.NoFiManagerEventNotifier;
import io.javalin.Javalin;

import java.util.Collections;

import static com.danisproductive.nofi.manager.server.http.routes.process.NoFiProcessServerRoutes.*;

public class NoFiProcessDeleteRoutes {
    public void setDeleteRoutes(Javalin server, NoFiManager manager) {
        setDeleteProcessTagRoute(server, manager);
        setDeleteProcessRoute(server, manager);
        setDeleteProcessInput(server, manager);
        setDeleteProcessOutput(server, manager);
    }

    private void setDeleteProcessTagRoute(Javalin server, NoFiManager manager) {
        server.delete(PROCESS_ROUTE + "/tag", context -> {
            String processId = context.queryParam(PARAM_PROCESS_ID);
            String tagName = context.queryParam(PARAM_TAG_NAME);
            manager.getProcess(processId).removeTag(tagName);
        });
    }

    private void setDeleteProcessInput(Javalin server, NoFiManager manager) {
        server.delete(PROCESS_ROUTE + "/input", context -> {
            String processId = context.queryParam(PARAM_PROCESS_ID);
            String inputName = context.queryParam(PARAM_INPUT_NAME);
            NoFiProcess process = manager.getProcess(processId);
            process.removeInput(inputName);
            NoFiManagerEventNotifier.sendInputDeleted(processId, inputName);
        });
    }

    private void setDeleteProcessOutput(Javalin server, NoFiManager manager) {
        server.delete(PROCESS_ROUTE + "/output", context -> {
            String processId = context.queryParam(PARAM_PROCESS_ID);
            String outputName = context.queryParam(PARAM_OUTPUT_NAME);
            NoFiProcess process = manager.getProcess(processId);
            process.removeOutput(outputName);
            NoFiManagerEventNotifier.sendOutputsDeleted(
                processId,
                Collections.singletonList(process.getOutputByName(outputName))
            );
        });
    }

    private void setDeleteProcessRoute(Javalin server, NoFiManager manager) {
        server.delete(PROCESS_ROUTE, context -> {
            String processId = context.queryParam(PARAM_PROCESS_ID);
            NoFiManagerEventNotifier.sendProcessDelete(manager.getProcess(processId));
            manager.deleteProcess(processId);
        });
    }
}
