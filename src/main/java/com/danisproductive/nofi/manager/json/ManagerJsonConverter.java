package com.danisproductive.nofi.manager.json;

import com.danisproductive.nofi.core.utils.json.NoFiCoreGsonBuilder;
import com.google.gson.Gson;

public class ManagerJsonConverter {
    public static Gson gson = new NoFiCoreGsonBuilder().build();
}
