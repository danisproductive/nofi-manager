package com.danisproductive.nofi.manager.queue;


import com.danisproductive.nofi.core.output.NoFiOutput;
import com.danisproductive.nofi.core.process.NoFiProcess;
import com.danisproductive.nofi.core.queue.NoFiQueue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class QueueManager {
    //todo check where this should be used (where queues are created probably)
    private Map<String, NoFiQueue<Object>> queues;


    public QueueManager() {
        queues = new HashMap<>();
    }

    public void addQueue(NoFiQueue<Object> queue) {
        queues.put(queue.getId(), queue);
    }

    public void removeQueue(String queueId) {
        queues.remove(queueId);
    }

    public NoFiQueue<Object> getQueue(String queueId) {
        return queues.get(queueId);
    }

    public void onProcessAdded(NoFiProcess process) {
        process.getOutputs().forEach(output -> {
            NoFiQueue queue = output.getQueue();

            if (queue != null) {
                addQueue(queue);
            }
        });
    }

    //todo find where to get old vs new info
    public void onProcessUpdated(NoFiProcess process) {
//        JsonObject processConfigJson = processJson.get(PROCESS_CONFIG.getValue()).getAsJsonObject();
//        JsonArray outputs = processConfigJson.get(PROCESS_OUTPUTS.getValue()).getAsJsonArray();
//        outputs.forEach(topic -> {
//            NoFiQueue<Object> queue = NoFiQueueFactory.getInstance().create(
//                    topic.getAsJsonObject(),
//                    queueManager.getQueueServerConfig());
//            queueManager.addQueue(queue);
//        });
    }

    public void onOutputDeleted(NoFiOutput output) {
        removeQueue(output.getQueue().getId());
    }

    public void onOutputsDeleted(List<NoFiOutput> outputs) {
        outputs.forEach(output -> removeQueue(output.getQueue().getId()));
    }
}
