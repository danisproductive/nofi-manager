package com.danisproductive.nofi.manager.queue.kafka;

import com.danisproductive.nofi.core.queue.kafka.config.NoFiKafkaConfig;
import com.danisproductive.nofi.core.queue.kafka.config.NoFiKafkaConsumerConfig;
import com.danisproductive.nofi.core.queue.kafka.config.NoFiKafkaProducerConfig;
import com.danisproductive.nofi.core.server.http.NoFiServerConfig;

public class NoFiQueueServerConfig extends NoFiServerConfig {
    private final NoFiKafkaConfig kafkaConfig;

    public NoFiQueueServerConfig(String kafkaBootstrapServers, String groupId) {
        NoFiKafkaConsumerConfig consumerConfig = new NoFiKafkaConsumerConfig();
        NoFiKafkaProducerConfig producerConfig = new NoFiKafkaProducerConfig();
        consumerConfig.setGroupId(groupId);
        this.kafkaConfig = new NoFiKafkaConfig(kafkaBootstrapServers, consumerConfig, producerConfig);
    }

    public NoFiKafkaConfig getKafkaConfig() {
        return kafkaConfig;
    }
}
