package com.danisproductive.nofi.manager;

import com.danisproductive.nofi.core.NoFiObject;
import com.danisproductive.nofi.core.process.NoFiProcess;
import com.danisproductive.nofi.core.tag.NoFiTag;
import com.danisproductive.nofi.core.utils.config.ConfigWriter;
import com.danisproductive.nofi.core.utils.config.FileConfigWriter;
import com.danisproductive.nofi.manager.json.ManagerJsonConverter;
import com.danisproductive.nofi.manager.process.NoFiProcessFactory;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

public class NoFiManager extends NoFiObject {
    private static final String DEFAULT_MANAGER_NAME = "NoFiManager";

    private String name;
    private Map<String, NoFiProcess> processes;
    private Map<String, NoFiTag> tags;
    private transient ConfigWriter<NoFiManager> configWriter;

    public NoFiManager() {
        configWriter = new FileConfigWriter<>("nofi-manager-data.json", ManagerJsonConverter.gson);
        name = DEFAULT_MANAGER_NAME;
        processes = new HashMap<>();
        tags = new HashMap<>();
    }

    public void setName(String name) {
        this.name = name;
        processes = new HashMap<>();
        tags = new HashMap<>();
    }

    public NoFiProcess addProcess(String type) {
        NoFiProcess newProcess = NoFiProcessFactory.create(type);

        if (newProcess != null) {
            processes.put(newProcess.getId(), newProcess);
        }

        return newProcess;
    }

    public NoFiProcess addProcess(NoFiProcess process) {
        processes.put(process.getId(), process);

        return process;
    }

    public NoFiProcess getProcess(String processId) {
        return processes.get(processId);
    }

    public Map<String, NoFiProcess> getProcesses() {
        return processes;
    }

    public void deleteProcess(String processId) {
        processes.remove(processId);
    }

    public NoFiTag createTag(String tagName) {
        NoFiTag tag;

        if (!tags.containsKey(tagName)) {
            tag = new NoFiTag(tagName);
            tags.put(tagName,tag);
        } else {
            return null;
        }

        return tag;
    }

    /**
     * Rename an existing tag
     *
     * @param originalName name of the tag we want to change
     * @param newName new name to be given to the tag
     * @return
     * @throws IllegalArgumentException
     */
    public NoFiTag renameTag(String originalName, String newName) throws IllegalArgumentException {
        if (!tags.containsKey(originalName)) {
            return null;
        } else {
            if (tags.containsKey(newName)) {
                return tags.get(originalName);
            } else {
                NoFiTag originalTag = tags.get(originalName);
                originalTag.setName(newName);
                tags.put(newName, originalTag);
                tags.remove(originalName);

                return originalTag;
            }
        }
    }

    public NoFiTag getTag(String name) {
        return tags.get(name);
    }

    public Map<String, NoFiTag> getTags() {
        return tags;
    }

    public boolean removeTag(String name) {
        if (!tags.containsKey(name)) {
            return false;
        } else {
            this.getProcesses().forEach((processId, process) -> {
                process.removeTag(name);
            });
            tags.remove(name);

            return true;
        }
    }

    public void setConfigWriter(ConfigWriter<NoFiManager> configWriter) {
        this.configWriter = configWriter;
    }

    public void save() throws FileNotFoundException {
        configWriter.save(this);
    }
}
